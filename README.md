ics-ans-librenms
===================

Playbook for setting up LibreNMS.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Group Variables
---------------

```yaml
traefik_dashboard_port: 8080
traefik_use_letsencrypt: false
```
Ansible-vault
-------------

Is used for encryption of passwords. In [AWX](https://torn.tn.esss.lu.se/) use the credential called ```ansible-vault```.
To run a local instance, create a ```.vault_pass``` file in the  edybook irectory with the ansible vault password, which can be found on the password page.

License
-------

BSD 2-clause
